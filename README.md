# SF5.2 Bootstrap project

This project is a Symfony 5.2 bootstrap to help you get started with a new Code-bay api. Once it is cloned or forked 
 locally, log in Code-bay and create a new API. Go to the api details section and define a SF5.2 code token. Copy it to
 clipboard in order to paste it to the "Bind Code-bay url" menu entry in PhpStorm.
 
 That's it ! you can now generate the code by selecting "Codebay generate code" menu entry in PhpStorm/VSCode. With this
 new code generated, you are ready to launch the app, type the following command :

    docker-compose -f docker-compose.dev.yml up -d
    docker exec -it cb_sf5-2_php bash
    composer install
    
Navigate to http://localhost:8889

enjoy :)


**NB :** If you encounter the following error when running "composer update" command line :

`
The file "../src/Api" does not exist (in: "/home/wwwroot/sf5-2/src/../config")
in /home/wwwroot/sf5-2/src/../config/services.yaml (which is being imported from
"/home/wwwroot/sf5-2/src/Kernel.php"). 
`

It means that you didn't generate default code from your Code-Bay IDE plugin.
To do so, check the docs explaining how to install and run it at https://code-bay.io/docs/ide-plugin.
